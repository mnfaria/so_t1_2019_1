#include "priothreads.h"
#include <pthread.h>
#include <stdlib.h>

typedef struct {
	unsigned int prioridade;
	unsigned int pid;
	//ainda falta coisa aqui
} pt_thread_ctx;

pt_thread_ctx *queue[8]; 
int cont[8];

void processador(unsigned int processadores){
/*vai ser o responsável por dar start nas threads.
responsável pelos 'processadores virtuais' */
}

void pt_init(unsigned int processadores){

	if (processadores > 8 || processadores < 1) //teste para valores incorretos
		processadores = 1;

	for(int i = 0; i < 8;i++){
		queue[i] = malloc(8*sizeof(pt_thread_ctx));	

		for (int j = 0; j < 8; j++){
			queue[i][j].prioridade = 0;
			queue[i][j].pid = 0;
		}
	}

	//processador(processadores);

}


void pt_spawn(unsigned int prioridade, void *(*funcao) (void *), void *parametros){
	pt_thread_ctx * thread; 

	if (prioridade > 8 || prioridade < 1) //teste para valores incorretos
		prioridade = 8;

	if (cont[prioridade-1] == 8){
		//fila cheia, decidir o que fazer
	} else {
		//fila vazia, colocar na fila de prioridade
		//pthread_create
		cont[prioridade-1] += 1; 
	}
	

}

/* Faz a thread atual liberar o processador, voltar ao fim da fila de sua prioridade e 
   esperar o próximo escalonamento 
*/
void pt_yield(){

}

/* Espera todas as threads terminarem */
void pt_barrier(){
	
}

void pt_destroy(){

	for(int i=0; i<8;i++)
		free(queue[i]);

	//laço de destroy das threads ativas
}
